#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

struct variables {
  int portno, sockfd, n, sread, s;
};

int main() {
  struct variables v;

  v.portno = 8080;
  char servhost[] = "127.0.0.1"; 

  char buffer[256];

  struct sockaddr_in serv_addr;
  struct hostent *server;

  // Create a socket for the client
  // If sockfd<0 there was an error in the creation of the socket
  if ((v.sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    puts("ERROR creating socket");
    return -1;
  }

  // Get the server address
  server = gethostbyname(servhost);

  // Clear the server address
  bzero((char *)&serv_addr, sizeof(serv_addr));

  // Set the socket family and address
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);

  // Set the port number
  serv_addr.sin_port = htons(v.portno);

  // Connect the client to the server
  if (connect(v.sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    puts("ERROR connecting to server");
    return -1;
  }

  printf("Connected to %s:%d\n", servhost, v.portno);

  // Send messages to the server and receive responses
  while (1) {
    fflush(stdout);
    buffer[v.sread] = '\0';

    printf(": ");
    bzero(buffer, 256);
    fgets(buffer, 255, stdin);

    // Send the message to the server
    if ((v.n = write(v.sockfd, buffer, strlen(buffer))) < 0) {
      puts("ERROR writing to socket");
      return -1;
    }

    bzero(buffer, 256);

    // Receive response from the server
    if ((v.s = read(v.sockfd, buffer, 255)) < 0) {
      puts("ERROR reading from socket");
      return -1;
    }

    // If the server sends "exit", close the socket and break out of the loop
    if (strncmp(buffer, "exit", 5) == 0) {
      close(v.sockfd);
      break;
    }

    printf("%s", buffer);
  }

  return 0;
}
