#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

struct variables {
  int sockfd, newsockfd, portno, n, nread, opt;
};

int main() {
  struct variables v;

  v.portno = 8080;

  socklen_t clilen;
  struct sockaddr_in serv_addr, cli_addr;

  char buffer[256];

  // Create a socket for the server
  if ((v.sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    puts("ERROR creating socket");
    return -1;
  }

  // Set the socket options
  if (setsockopt(v.sockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &v.opt, sizeof(v.opt)) != 0) {
    return -1;
  }

  // Clear the server address
  bzero((char *)&serv_addr, sizeof(serv_addr));

  // Set the server address and port
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(v.portno);

  // Bind the socket to the server address
  if (bind(v.sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    puts("ERROR binding socket");
    return -1;
  }

  // Listen for incoming connections
  printf("Listening on port %d\n", v.portno);
  listen(v.sockfd, 5);

  // Accept a connection from a client
  v.newsockfd = accept(v.sockfd, (struct sockaddr *)&cli_addr, &clilen);
  clilen = sizeof(cli_addr);

  // Receive messages from the client and send responses
  while (1) {
    fflush(stdout);
    buffer[v.nread] = '\0';

    // If there is an error accepting the connection, print an error message and exit
    if (v.newsockfd < 0) {
      puts("ERROR accepting connection");
      return -1;
    }

    // Clear the buffer
    bzero(buffer, 256);

    // Receive a message from the client
    if ((v.n = read(v.newsockfd, buffer, 255)) < 0) {
      puts("ERROR reading from socket");
      return -1;
    }

    // Print the message
    printf("%s", buffer);

    // Send a response to the client
    if ((v.n = write(v.newsockfd, "[+]", 3)) < 0) {
      puts("ERROR writing to socket");
      return -1;
    }

    // If the client sends "exit", close the sockets and break out of the loop
    if (strncmp("exit", buffer, 4) == 0) {
      v.n = write(v.newsockfd, "Server Exit", 11);
      close(v.newsockfd);
      close(v.sockfd);
      break;
    }
  }

  return 0;

}
