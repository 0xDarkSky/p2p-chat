# p2p-chat

C socket implementation with sending and receiving text

![PREVIEW](https://i.imgur.com/Fu7H5ON.png)

# To-do:

- Threading with send and receive on both sides
- Send files
- Encryption?

# Build

Run `build.sh`
Make sure to edit both files to your port and server

```c
// client.c
char servhost[] = "127.0.0.1"; 
v.portno = 8080;           
```

```c
// server.c
v.portno = 8080; 
```
